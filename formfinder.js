
function getTextFieldNames()
{
    let textFieldsNames=[];
    let textFields;
    let formsCollection = document.getElementsByTagName("form");
    for (let i = 0; i < formsCollection.length; i++) {
        // TODO: Check for all input types other than ones which cannot be made to look like text.
        // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
        textFields=formsCollection[i].querySelectorAll("input[type='text']");
        for (let j=0; j < textFields.length; j++)
        {
            textFieldsNames.push(textFields[j].name);
        }
    }
    return textFieldsNames;
}

// Input is of format {name: "", value: ""}
function setTextFieldsWithValues(textFieldNamesValues)
{
    let elementsByName;
    for (let i = 0; i < textFieldNamesValues.length; i++) {
        elementsByName = document.getElementsByName("textFieldNamesValues[i].name");
        // TODO: Problematic - it sets the first element of that name to be that value - may be check if it's a text
        // TODO: Make sure that only the right elements are set to the ciphertext values.
        elementsByName[0] = textFieldNamesValues[i].value;
    }
}