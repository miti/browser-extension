// https://stackoverflow.com/questions/40645538/communicate-data-from-popup-to-content-script-injected-by-popup-with-executescri
let derivedKeyGlobal;
let ownPublicKeyGlobalTesting;

console.log('from content script');

async function sendFieldsForEncryption(fields_array, regenerateOwnPublicKeyBoolean, getOwnPublicKeyBoolean)
{
  let message={"url": document.URL, "fields" : fields_array};
  if(regenerateOwnPublicKeyBoolean)
    message.regenerateOwnPublicKey = true;
  if(getOwnPublicKeyBoolean)
    message.getOwnPublicKey = true;
  try {
      returnValue = await browser.runtime.sendMessage(message);
    }
    catch(err) {
      console.log("Error in browser.runtime.sendMessage in sendFieldsForEncryption");
      console.log(err);
      returnValue={error: err};
    };

  if(returnValue.error)
  {
    console.log(returnValue.error);
    return returnValue;
  }
  return returnValue;
}

// This function assumes that there is just 1 eventlistener, itself
// In case of multiple event handlers, we need to make sure that the 1) other event handlers handle the user data in a privacy-preserving way and
// 2) usability - this event handler is executed after all other ones that expect plaintext data (if there are handlers run chronologically after it, then they need to send the current - encrypted - content of the form data)
async function handleButtonClick(event)
{
  event.preventDefault();
  console.log("Helloworld");
  let dataObject = {};
  let name = document.getElementById("name").value;
  let age = document.getElementById("age").value;
  dataObject["name"] = name;
  dataObject["age"] = age;

  // Encrypt name and age
  let fields = Object.keys(dataObject);
  let returnValue = await sendFieldsForEncryption([...Object.values(dataObject)], true, true);
  if(returnValue.error)
    return;
  let encryptedFields=returnValue.ciphertextFields;
  let counter=0;
  for(obj of fields)
  {
    document.getElementById(obj).value=encryptedFields[counter];
    console.log(document.getElementById(obj).value);
    counter++;
  }
  console.log("Done with buttonclick handling");

  document.getElementById("form").submit();
}

function getRandomString(length)
{
  let randomString="";
  do {
    randomString += Math.random().toString(36).substring(2);
  } while (randomString.length < length+1);
  return randomString.substring(0,length);
}

async function handleNButtonClicks(event)
{
  event.preventDefault();
  const url=document.URL + "action.php";
  const time_measurements = [];
  const number_of_trials=10;
  console.log("Hello starting this many trials for case with native server or with SGX:" + number_of_trials);
  for(let counter=0; counter<number_of_trials; counter++)
  {
    const randomName=getRandomString(30);
    const randomAge=getRandomString(30);
    const post_data = `name=${randomName}&age=${randomAge}`; // JSON.stringify({name: randomName, age: randomAge});
    const old_time = Date.now();
    const ball = await fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: post_data
    });
    const new_time = Date.now();
    const response_text= await ball.text();
    [returnedName, returnedAge] = response_text.slice(12,-12).trim().split(" ");
    if(returnedName !== randomName || returnedAge !== randomAge)
    {
      console.log("Didnt get the right name and age.");
      console.log(returnedName);
      console.log(returnedAge);
      console.log(randomName);
      console.log(randomAge);
      break;
    }
    const time_lag = new_time - old_time;
    time_measurements.push(time_lag);
    if(counter % (number_of_trials/10) === 0)
    {
      console.log(time_measurements);
    }
  }
  console.log("Done");
  compute_stats_on_array(time_measurements);
}

async function handleNSgxButtonClicks(event)
{
    sendOrLogSgxRequests(event, false, 100, 5);
    //logSgxRequests(event, 1000, 1);
}

async function logSgxRequests(event, number_of_trials, maxNumberOfFormFields)
{
  const send_url=document.URL + "/action.php";
  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
  let expected_response_string = "";
  let curl_string = "";
  let numberOfFormFields=1;
  let formFieldCounter=0;

  for(numberOfFormFields=1; numberOfFormFields<=maxNumberOfFormFields; numberOfFormFields++)
  {
    for(let counter=0; counter<number_of_trials; counter++)
    {
        let expected_response="";
        let arrayOfFormFields = [];
        // Initializing the array of form fields, of numberOfFormFields length, with random values.
        expected_response+="<html><body> ";
        for(formFieldCounter=0; formFieldCounter<numberOfFormFields; formFieldCounter++)
        {
          const formField=getRandomString(15);
          expected_response+=formField + " ";
          arrayOfFormFields.push(formField);
        }
        expected_response+="</body></html>\n";
        expected_response_string+=expected_response;
        // Encrypting the array of form fields
        //  encryptionStartTime=Date.now();
        returnValue = await sendFieldsForEncryption(arrayOfFormFields, false, true);
        if(returnValue.error)
          break;
        // encryptionEndTime=Date.now();
        // encryptionTimeMeasurements[numberOfFormFields-1][counter]=(encryptionEndTime - encryptionStartTime);

        // Initializing the ciphertext array of form fields
        let ciphertextArrayOfFormFields=returnValue.ciphertextFields;
        let post_data="";
        for(formFieldCounter=0; formFieldCounter<numberOfFormFields; formFieldCounter++)
        {
          const encodedCiphertext=encodeURIComponent(ciphertextArrayOfFormFields[formFieldCounter]);
          post_data+=`f${formFieldCounter+1}=${encodedCiphertext}&`;
        }

        post_data=post_data.slice(0,-1);
        curl_string += post_data +"\n";
      }
      if(returnValue.error)
      {
          console.log("breaking");
          break;
      }
    }
    if(returnValue.error)
      return; // woulda been logged at this point
    console.log(expected_response_string);
    console.log(curl_string);
    expected_response_string="";
    curl_string="";

    //let localStorageObject=window.localStorage;
    //localStorageObject.setItem('expected_response', expected_response_string);
    //localStorageObject.setItem('curl_string', curl_string);
}

async function sendOrLogSgxRequests(event, sendRequestsBoolean, number_of_trials, maxNumberOfFormFields)
{
  console.log("Starting tests for this many trials for SGX extension case: " + number_of_trials);
  console.log("sendRequestsBoolean is " + sendRequestsBoolean);

  let expected_response_string = "";
  let curl_string = "";
  let networkTimeMeasurements="";
  let encryptionTimeMeasurements="";
  let noMitigatorTimeMeasurements="";
  let noSgxTimeMeasurements="";
  let returnValue, encryptionStartTime, encryptionEndTime, networkSendTime, networkReceiveTime;
  let numberOfFormFields=1;
  let formFieldCounter=0;

  const send_url=document.URL + "/action.php";
  const nomitigator_url="http://crysp-server1.cs.uwaterloo.ca:8045/action.php";
  const nosgx_url="http://crysp-server1.cs.uwaterloo.ca:8046/action.php";
  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

  for(let counter=0; counter<number_of_trials; counter++)
  {
    // For each of the number_of_trials run of the experiment, do it with a different number of form fields being sent each time.
    for(numberOfFormFields=1; numberOfFormFields<=maxNumberOfFormFields; numberOfFormFields++)
    {
      let expected_response="";
      let arrayOfFormFields = [];

      // Initializing the array of form fields, of numberOfFormFields length, with random values.
      expected_response+="<html><body> ";
      for(formFieldCounter=0; formFieldCounter<numberOfFormFields; formFieldCounter++)
      {
        const formField=getRandomString(32);
        expected_response+=formField + " ";
        arrayOfFormFields.push(formField);
      }
      expected_response+="</body></html>\n";

      // Encrypting the array of form fields
      encryptionStartTime=Date.now();
      returnValue = await sendFieldsForEncryption(arrayOfFormFields, false, true);
      encryptionEndTime=Date.now();
      if(returnValue.error)
        break;
      encryptionTimeMeasurements+=encryptionStartTime + " " + encryptionEndTime + "\n";

      // Initializing the ciphertext array of form fields
      let ciphertextArrayOfFormFields=returnValue.ciphertextFields;
      let post_data="";
      for(formFieldCounter=0; formFieldCounter<numberOfFormFields; formFieldCounter++)
      {
        const encodedCiphertext=encodeURIComponent(ciphertextArrayOfFormFields[formFieldCounter]);
        post_data+=`f${formFieldCounter+1}=${encodedCiphertext}&`;
      }
      post_data=post_data.slice(0,-1);

      ownPublicKey=returnValue.ownPublicKey.ownPublicKey;

      if(sendRequestsBoolean)
      {
        // Setting header and sending request
        myHeaders.set("Mitigator-Client-Public-Key", ownPublicKey);
        networkSendTime = Date.now();
        const ball = await fetch(send_url, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          headers: myHeaders,
          body: post_data
        });
        networkReceiveTime = Date.now();
        networkTimeMeasurements+=networkSendTime + " " + networkReceiveTime +"\n";
        const response_text= await ball.text();

        // Checking returned values.
        let returnedTextsArray = response_text.slice(12,-14).trim().split(" ");
        for(formFieldCounter=0; formFieldCounter<numberOfFormFields; formFieldCounter++)
        {
          if(returnedTextsArray[formFieldCounter] !== arrayOfFormFields[formFieldCounter])
          {
            returnValue = {error: "Didnt get the right name and age."};
            console.log("Didnt get the right name and age.");
            console.log(returnedTextsArray[formFieldCounter]);
            console.log(arrayOfFormFields[formFieldCounter]);
            break;
          }
        }

        // Sending identical values to the Graphene-SGX server.
        networkSendTime = Date.now();
        let ball2 = await fetch(nomitigator_url, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          headers: myHeaders,
          body: post_data
        });
        networkReceiveTime = Date.now();
        noMitigatorTimeMeasurements+=networkSendTime + " " + networkReceiveTime +"\n";

        // Sending identical values to the control server.
        networkSendTime = Date.now();
        ball2 = await fetch(nosgx_url, {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          headers: myHeaders,
          body: post_data
        });
        networkReceiveTime = Date.now();
        noSgxTimeMeasurements+=networkSendTime + " " + networkReceiveTime +"\n";
      }
      else {
        expected_response_string+=expected_response;
        curl_string += post_data +"\n";
      }
    }
    if(returnValue.error)
      {
        console.log("breaking");
        break;}
  }
  if(returnValue.error)
    return; // woulda been logged at this point

  console.log("Done");
  if(sendRequestsBoolean)
  {
    console.log("Network times");
    console.log(JSON.stringify(networkTimeMeasurements));
    console.log("Network times for no Mitigator case");
    console.log(JSON.stringify(noMitigatorTimeMeasurements));
    console.log("Network times for no SGX case");
    console.log(JSON.stringify(noSgxTimeMeasurements));
    console.log("Encryption times");
    console.log(JSON.stringify(encryptionTimeMeasurements));
  }
  else
  {
    console.log(expected_response_string);
    console.log(curl_string);
  }
}

document.getElementById("form").addEventListener("submit", handleButtonClick, { once: true});
document.getElementById("experimenter1Button").addEventListener("click", handleNButtonClicks);
document.getElementById("experimenter2Button").addEventListener("click", handleNSgxButtonClicks);
