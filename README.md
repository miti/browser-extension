# MITIGATOR - Client-side extension

This repository contains the source code for the browser extension for the paper: 

Miti Mazmudar, Ian Goldberg. "Mitigator: Privacy policy compliance using trusted hardware".  
Proceedings on Privacy Enhancing Technologies. Vol. 2020, No. 3. 18 pages. July 2020.

This browser extension essentially encrypts form field content to an SGX server-side enclave (the decryptor enclave) for a Mitigator-supporting website. The content script handles events on the page --- it passes form fields to the background script for encryption when a user presses the submit button. The background script maintains the client's and Mitigator-supporting website's keys, encrypts form fields with these keys, listens to incoming HTTP headers for the `Mitigator-Public-Key` header and inserts the client's own header `Mitigator-Public-Key`. 

## Installation instructions
This extension has been tested for Firefox version 73.0.1. First, clone the extension using the above link. To install the extension into Firefox, go to the `about:debugging#/runtime/this-firefox`  tab. Click on the 'Load Temporary Add-on...' button under 'Temporary Extensions'. Select and load the `manifest.json` file from the cloned project.  

The background script logs the remote decryptor enclave's ephemeral public key, its own ephemeral public key, the shared secret, as well as plaintext, ciphertext and base64 form field values. This logged output can be observed by opening the Browser Console in Firefox.   